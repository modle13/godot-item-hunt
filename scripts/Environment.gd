extends Node

export (PackedScene) var item_scene = preload("res://scenes/Item.tscn")
export (PackedScene) var tree_scene = preload("res://scenes/Tree.tscn")
export (PackedScene) var stone_scene = preload("res://scenes/Stone.tscn")
export (PackedScene) var chest_scene = preload("res://scenes/Chest.tscn")

onready var scenery_settings = {
    'Tree':   {'max': 1500, 'spread': 1, 'cluster': 30, 'parent_node': $Tree,   'scene': tree_scene},
    'Stone':  {'max': 750,  'spread': 1, 'cluster': 25, 'parent_node': $Stone,  'scene': stone_scene},
    'Copper': {'max': 600,  'spread': 1, 'cluster': 20,  'parent_node': $Copper, 'scene': stone_scene},
    'Iron':   {'max': 500,  'spread': 1, 'cluster': 10,  'parent_node': $Iron,   'scene': stone_scene},
}

var spawn_extent = {'x': {'min': -576, 'max': 192}, 'y': {'min': -192, 'max': 576}}
var cluster_variance = 0.3
var spawn_positions = []
onready var map = get_node("../Map")


func _ready():
    randomize()
    generate_scenery()


func generate_scenery():
    for key in scenery_settings:
        pass
        generate_harvestable_object(key, scenery_settings[key])


func generate_harvestable_object(type, settings):
    var rng = RandomNumberGenerator.new()
    rng.randomize()

    var container = find_node(type)
    for n in settings.get('max'):
        var spawn_position = Vector3(
            rng.randi_range(spawn_extent.x.min, spawn_extent.x.max),
            0,
            rng.randi_range(spawn_extent.y.min, spawn_extent.y.max)
        )

        for m in rng.randi_range(settings.cluster * -cluster_variance, settings.cluster * cluster_variance):
            if container.get_children().size() >= settings.get('max'):
                return
            var object = settings.get('scene').instance()
            spawn_position.x += rng.randi_range(-10, 10)
            spawn_position.z += rng.randi_range(-10, 10)

            # prevent crowding out player at spawn
            if abs(spawn_position.x) < 2 or abs(spawn_position.z) < 2:
                continue

            # prevent adding duplicate position spawns
            if spawn_positions.has(spawn_position):
                continue

            spawn_positions.append(spawn_position)
            object.initialize(type, spawn_position)
            # rotate object to random degree
            object.rotation_degrees.y = rand_range(0, 360)
            object.connect("rekd", self, "_on_Object_rekd", [object])
            object.find_node('Pivot').find_node('Default').hide()
            object.find_node('Pivot').find_node(type).show()
            container.add_child(object)
            object.map_indicator = map.add_object(type, Vector2(spawn_position.x, spawn_position.z), Vector2(8, 8))
            object.resource_type = type


func _on_Object_rekd(object):
    for i in range(5):
        randomize()
        var z_pos = rand_range(object.translation.z - 2, object.translation.z + 2)
        var x_pos = rand_range(object.translation.x - 2, object.translation.x + 2)
        if object.resource_type == 'tree':
            x_pos = rand_range(object.translation.x - 100, object.translation.x + 100)
        var new_pos = Vector3(x_pos, object.translation.y + 0.5, z_pos)
        spawn_item(new_pos, object.resource_type)

    object.map_indicator.queue_free()
    print("triggered _on_Object_rekd ", object.translation, " ", object.resource_type)


func spawn_item(spawn_position, type):
    var item = item_scene.instance()

    item.find_node('Pivot').find_node('Default').hide()
    item.find_node('Pivot').find_node(type).show()

    item.get_node("AnimationPlayer").stop()
    item.get_node("AnimationPlayer").play("flat", -1, 0.1)

    get_node("Items").add_child(item)

    item.initialize(spawn_position)
    # this connects the item's signal 'collected'
    #   to the function "_on_Item_collected"
    #   on the $UserInterface/Inventory node
    item.connect("collected", get_parent().get_node("Inventory"), "_on_Item_collected", [type])
