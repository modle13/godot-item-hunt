extends DirectionalLight

var light_shadow_dist : float = 200.0


func _ready():
    directional_shadow_max_distance = light_shadow_dist
