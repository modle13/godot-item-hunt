extends KinematicBody

# emit this when dead to use to count the player score
signal collected


var velocity = Vector3.ZERO
var detected_player = false
var target_position = Vector3.ZERO
var velocity_multiplier = 5


# leading underscore tells compiler we're not using that argument
func _physics_process(_delta):
    if detected_player:
        velocity = target_position - translation
        velocity = Vector3(velocity.x, 0, velocity.z)
    else:
        velocity = Vector3.ZERO

    velocity = move_and_slide(velocity * velocity_multiplier)
    for index in get_slide_count():
        # other colliders this object has slid against on movement
        var collision = get_slide_collision(index)
        if collision.collider.is_in_group("player"):
            collect()


func initialize(spawn_position):
    translation = spawn_position


func collect():
    emit_signal("collected")
    queue_free()


func _on_VisibilityNotifier_screen_exited():
    # enqueues object to be removed if off screen
#    queue_free()
    pass


func _on_PlayerDetector_body_entered(body):
    if body.name == 'Player':
        target_position = body.translation
        detected_player = true


func _on_PlayerDetector_body_exited(body):
    if body.name == 'Player':
        detected_player = false
