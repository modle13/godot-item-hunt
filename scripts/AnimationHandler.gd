extends Spatial

var animation_player = null
var type_name = ""
# key is used in code, value is name of animation in AnimationPlayer
var capsule_animation_map = {
    "attack": "strike",
    "idle": "idle,",
    "Mlem": "mlem",
    "run": "run",
}
var fox_animation_map = {
    # animations are [Attack, Fall, Idle, Jump, Run, ToucheGround]
    "idle":   "Idle",
    "attack": "Attack",
    "jump":   "Jump",
    "run":    "Run",
}
var frog_animation_map = {
    # animations are [Attack, Fall, Idle, Jump, Run, ToucheGround]
    "idle":   "FrogArmature|Frog_Idle",
    "attack": "FrogArmature|Frog_Attack",
    "jump":   "FrogArmature|Frog_Jump",
    "run":    "FrogArmature|Frog_Jump",
}
var map_name_map = {
    "capsule": capsule_animation_map,
    "fox": fox_animation_map,
    "frog": frog_animation_map,
}
var animation_map = {}


func init_animation_player(map_name, source_obj):
    type_name = map_name
    # default to fox animation
    animation_map = map_name_map.get(map_name, fox_animation_map)
    animation_player = source_obj
#    print(animation_player.get_animation_list())
    set_animation_player_defaults()


func set_animation_player_defaults():
    # animation_player.play(animation_map.idle)
    # animation_player.get_animation(animation_map.idle).set_loop(true)
    # animation_player.set_blend_time(animation_map.attack, animation_map.idle,   0.1)
    # animation_player.set_blend_time(animation_map.idle,   animation_map.attack, 0.4)
    # animation_player.set_blend_time(animation_map.idle,   animation_map.jump,   0.1)
    # animation_player.set_blend_time(animation_map.jump,   animation_map.idle,   0.1)
    # animation_player.set_blend_time(animation_map.run,    animation_map.jump,   0.1)
    # animation_player.set_blend_time(animation_map.jump,   animation_map.run,    0.1)
    # animation_player.set_blend_time(animation_map.run,    animation_map.idle,   0.2)
    # animation_player.set_blend_time(animation_map.idle,   animation_map.run,    0.2)
    pass


## from docs: void play(name: String = "", custom_blend: float = -1, custom_speed: float = 1.0, from_end: bool = false)
func play(anim, blend=-1, speed=1.0):
#    print('anim is ', animation_map.get(anim), ', speed is ', speed)
    animation_player.play(animation_map.get(anim), blend, speed)


func get_current_anim():
    var current = animation_player.current_animation
    for entry in animation_map:
        if animation_map[entry] == current:
            return entry
    return ""
