extends Node2D

signal mouse_entered_cell
signal mouse_exited_cell

var mouse_item = null
var mouse_swap_item = null


func check_swap_item():
    if mouse_item.get('name') == mouse_swap_item.get('name'):
        mouse_item['count'] += mouse_swap_item.get('count')
        mouse_swap_item = null


func _on_Display_mouse_entered():
    emit_signal("mouse_entered_cell")


func _on_Display_mouse_exited():
    emit_signal("mouse_exited_cell")
