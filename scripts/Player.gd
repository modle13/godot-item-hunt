extends KinematicBody

signal hit
signal attack_finished

# exported variables can be edited in the editor
export var default_speed = 15
export var speed = 15
export var run_speed = 45
export var jump_impulse = 20.0
export var fall_acceleration = 50
export var bounce_impulse = 16.0
var spawn_position = Vector3(0, 5, 0)

var attackable_target
var interactable_target
var current_target

onready var animation_run = $AnimationRun
onready var animation_strike = $AnimationStrike

onready var inventory = get_node("../Inventory")
onready var mouse_item_cell = get_node("../MouseItemCell")
var menu_mode = false
var placement_mode = false
var map_mode = false

var direction = Vector3.ZERO
var velocity = Vector3.ZERO
var jump_velocity = Vector3.ZERO

var skills = {
    "attacking": 0,
    "jumping": 0,
    "map_reading": 0,
    "running": 0,
}

onready var skills_cell = get_tree().get_root().get_node("Main").get_node("Skills").get_node("Label")


func _ready():
    pass
    # animation_attack.init_animation_player("capsule", $AnimationAttack)
    # animation_run.init_animation_player("capsule", $AnimationRun)


func _physics_process(delta):
    # called 60 times per second by default
    # similar to _process but for physics, such as RigidBody or KinematicBody
    var moved = handle_lateral_movement(delta)
    if not moved:
        handle_idle()

    current_target = check_raycast("any")
    if not current_target:
        clear_interactable()

    handle_vertical_movement(delta)

    if velocity != Vector3.ZERO:
        # probably a better way; reset x and y to avoid returns
        # from move_and_slide causing actual unwanted sliding
        velocity = move_and_slide(velocity, Vector3.UP)

    check_for_item_collisions()

    if menu_mode or placement_mode:
        return


func _input(event):
    """Handle input trigger events."""
    # toggle_debug binding is 'E' (Project > Project Settings > Input Map)
    if Input.is_action_just_pressed("interact") and not placement_mode:
        if not interactable_target:
            interactable_target = check_raycast("interactable")
        if interactable_target:
            # toggle interactable if visible
            var interactable_visible = interactable_target.get_node("Inventory").toggle()
            interactable_target.toggle(interactable_visible)
            manage_inventory_mode(interactable_visible)
        else:
            # else just manage player inventory
            manage_inventory_mode(!inventory.visible)

    # toggle_debug binding is 'I' (Project > Project Settings > Input Map)
    if event.is_action_pressed("inventory_toggle") and not placement_mode and not map_mode:
        manage_inventory_mode(!inventory.visible)
        clear_interactable()

    # toggle_debug binding is 'ESC' (Project > Project Settings > Input Map)
    if event.is_action_pressed("menu_close"):
        set_menu_mode(false)

    # toggle_debug binding is 'Home' (Project > Project Settings > Input Map)
    if event.is_action_pressed("warp_home"):
        translation = spawn_position

    # mouse motion detection when inventory is open
    if event is InputEventMouseMotion and inventory.visible and mouse_item_cell.mouse_item != null:
        var mouse_delta = event.relative
        mouse_item_cell.position += mouse_delta

    # toggle_debug binding is 'LMB' (Project > Project Settings > Input Map)
    if Input.is_action_just_pressed("attack") and not placement_mode:
        animation_strike.play("strike", 0.1, 2)
        if not attackable_target:
            attackable_target = check_raycast("attackable")
            if attackable_target and attackable_target.attackable:
                attackable_target.attacked()
                attackable_target = null
                skills.attacking += 1

    set_skills()


func set_skills():
    var out = ""
    for entry in skills:
        out += "%s: %s\n" % [entry, skills.get(entry)]
    skills_cell.text = out


func set_placement_mode(mode):
    placement_mode = mode
    if placement_mode:
        manage_inventory_mode(false)
        clear_interactable()


func manage_inventory_mode(is_visible):
    inventory.set_visible(is_visible)
    set_menu_mode(inventory.visible)


func clear_interactable():
    if interactable_target:
        print('clearing interactable, ', interactable_target)
        interactable_target.get_node("Inventory").set_visible(false)
        # close it if it's open
        interactable_target.toggle(false)
        # clear player reference to interactable target
        interactable_target = null
        manage_inventory_mode(false)


func set_menu_mode(mode):
    menu_mode = mode
    set_mouse_mode()


func set_mouse_mode():
    if menu_mode:
        if mouse_item_cell.mouse_item != null:
            Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
        else:
            Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
        $CameraPivot.movement_active = false
    else:
        Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
        $CameraPivot.movement_active = true


func check_raycast(group):
    # get world space state from physics engine
    var space_state = get_world().direct_space_state
    # determine start and end position of ray
    # use basis to project along facing orientation in world's 3D space
    var check_end_pos = translation + transform.basis.z * -5
    # cast the ray
    var result = space_state.intersect_ray(translation, check_end_pos)
    # check result for intersections
    if result and (result.collider.is_in_group(group) or group == 'any'):
        if result.collider.get_parent().name != 'BuilderScenes':
            return result.collider


func handle_lateral_movement(delta):
    var input = get_lateral_input()

    if input == Vector3.ZERO:
        velocity.x = 0
        velocity.z = 0
        animation_run.stop()
        return false

    if is_on_floor():
        animation_run.play("run")

    # basis is the orientation in 3D space, the look direction
    # want to treat y axis differently from x and z so we calculate these separately
    direction = (transform.basis.z * input.z + transform.basis.x * input.x)

    if Input.is_key_pressed(KEY_SHIFT):
        speed = run_speed
        if velocity.x != 0 or velocity.z != 0:
            skills.running += 0.05
    else:
        speed = default_speed

    velocity.x = direction.x * speed
    velocity.z = direction.z * speed

    # turn character to face direction
    # `translation` makes it relative to player
    # without this it would face the scene origin + `direction`
    $Pivot.look_at(translation + direction, Vector3.UP)

    return true


func get_lateral_input():
    var input = Vector3.ZERO

    if Input.is_action_pressed("move_right"):
        input.x += 1
    if Input.is_action_pressed("move_left"):
        input.x -= 1

    if Input.is_action_pressed("move_back"):
        input.z += 1
    if Input.is_action_pressed("move_forward"):
        input.z -= 1

    input = input.normalized()

    return input


func handle_idle():
    pass


func handle_vertical_movement(delta):
    # toggle_debug binding is 'Spacebar' (Project > Project Settings > Input Map)
    if is_on_floor() and Input.is_action_just_pressed("jump"):
        velocity.y += jump_impulse
        skills.jumping += 1
    velocity.y -= fall_acceleration * delta


func check_for_item_collisions():
    # the number of times your node collided when calling move_and_slide in this frame
    for index in get_slide_count():
        var collision = get_slide_collision(index)
        if collision.collider.is_in_group("item"):
            var other = collision.collider
            other.collect()


func die():
    emit_signal("hit")


func _on_MobDetector_body_entered(_body):
    print('hit the thing')
    die()
