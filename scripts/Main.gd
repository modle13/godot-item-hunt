extends Node

onready var player = get_node("Player")


func _input(event):
    # toggle_debug binding is 'F3' (Project > Project Settings > Input Map)
    if Input.is_action_just_pressed("toggle_debug"):
        get_node("Environment/Ground").visible = !get_node("Environment/Ground").visible

    # toggle_debug binding is 'M' (Project > Project Settings > Input Map)
    if Input.is_action_just_pressed("toggle_map"):
        var visible = get_node("Map").toggle()
        player.map_mode = visible
        player.skills.map_reading += 1
