extends Spatial

export (PackedScene) var barrel_scene           = preload("res://scenes/Barrel.tscn")
export (PackedScene) var chest_scene            = preload("res://scenes/Chest.tscn")
export (PackedScene) var furnace_scene          = preload("res://scenes/Furnace.tscn")
export (PackedScene) var part_scene             = preload("res://scenes/Part.tscn")

# Will have an inventory-like menu to select target build object from.
onready var build_objects = {
    'Barrel':  {'node': get_node("BuilderScenes/Barrel"),  'scene': barrel_scene,},
    'Chest':   {'node': get_node("BuilderScenes/Chest"),   'scene': chest_scene,},
    'Furnace': {'node': get_node("BuilderScenes/Furnace"), 'scene': furnace_scene,},
    'Part':    {'node': get_node("BuilderScenes/Part"),    'scene': part_scene,},
}
onready var player = get_parent()
onready var environment = player.get_node("../Environment")
onready var inventory = get_node("Inventory")

var current_build = null
var current_build_node = null
var build_mode = false
var mouse_multiplier = 0.02
var current_rotation_degrees = 0
# 22.5 degrees is 1/16th of a circle
var adjustment_degrees = 22.5
var vertical_adjustment = 0.05
var hidden_height = -100


func _ready():
    select_build_object('Furnace')


func _physics_process(delta):
    if build_mode:
        check_raycast("y", 0, -20)


func select_build_object(build_object_name):
    for entry in build_objects:
        set_object_detector_disabled(build_objects[entry].node, true)
        build_objects[entry].node.visible = false
        build_objects[entry].node.translation.y = hidden_height
        build_objects[entry].node.translation.z = 2

    current_build = build_objects.get(build_object_name)
    if not current_build:
        print("error getting build object for ", build_object_name, ", probably forgot to update build_objects dict")
        return

    current_build_node = current_build.node

    current_build_node.translation.y = current_build_node.starting_y
    set_object_detector_disabled(current_build_node, false)
    if build_mode:
        # need to update the rotation to avoid appearance of rotation jittering on switch
        update_rotation()
        current_build_node.visible = true


func set_object_detector_disabled(target_node, target_state):
    var object_detector = target_node.get_node('ObjectDetector/CollisionShape')
    object_detector.set_disabled(target_state)


func update_rotation():
    # rotate build object by amount captured by mouse wheel operation
    current_build.get('node').rotation_degrees.y = -player.rotation_degrees.y + current_rotation_degrees


func check_raycast(dir, low, high):
    if self.visible:
        var low_vector = Vector3.ZERO
        var high_vector = Vector3.ZERO
        if dir == "x":
            low_vector.x = low
            high_vector.x = high
        if dir == "y":
            low_vector.y = low
            high_vector.y = high
        if dir == "z":
            low_vector.z = low
            high_vector.z = high
        # get world space state from physics engine
        var space_state = get_world().direct_space_state
        # determine start and end position of ray
        var check_start_pos = current_build_node.translation + low_vector
        var check_end_pos = current_build_node.translation + high_vector
        # cast the ray
        var result = space_state.intersect_ray(check_start_pos, check_end_pos)
        # check result for intersections
        if result:
            # set position to intersection position
            current_build_node.translation = result.position


func _input(event):
    """Handle input trigger events."""
    if event.is_action_pressed("build_mode"):
        # show object in build mode
        build_mode = !build_mode
        print('build mode toggled')
        player.set_placement_mode(build_mode)
        if current_build_node != null:
            current_build_node.visible = build_mode
            update_rotation()

    if event.is_action_pressed("open_build_menu") and build_mode:
        set_build_inventory_mode(!inventory.visible)

    if current_build_node == null:
        return

    if event.is_action_pressed("attack") and build_mode and !inventory.visible:
        # place the object
        if player.menu_mode:
            return
        if current_build_node.object_collided:
            print('too close to another object, cannot build')
            return
        var build_instance = current_build.scene.instance()
        build_instance.initialize(current_build_node.get_global_transform()[3])
        build_instance.rotation_degrees.y += current_rotation_degrees
        environment.add_child(build_instance)

    if event is InputEventMouseMotion and build_mode:
        # move object in build mode
#        var new_pos = current_build_node.translation.z + event.relative.y * mouse_multiplier
#        # keep build object within specified range via clamp
#        current_build_node.translation.z = clamp(new_pos, -4, 3)
        update_rotation()
        # return avoids processing multiple rotations if wheel is also moving
        return

    if build_mode:
        if Input.is_action_just_released("wheel_up"):
            if Input.is_key_pressed(KEY_CONTROL):
                current_build_node.translation.y += vertical_adjustment
            else:
                adjust_current_rotation(1)
        if Input.is_action_just_released("wheel_down"):
            if Input.is_key_pressed(KEY_CONTROL) and false:
                current_build_node.translation.y -= vertical_adjustment
            else:
                adjust_current_rotation(-1)


func set_build_inventory_mode(new_mode):
    # toggle the build menu inventory
    var build_inventory_mode = new_mode
    if new_mode:
        build_inventory_mode = inventory.toggle()
    else:
        inventory.set_visible(new_mode)
    # this yield avoids placing an build item immediately on menu close
    yield(get_tree().create_timer(0.05), "timeout")
    player.set_menu_mode(build_inventory_mode)


func adjust_current_rotation(direction):
    current_rotation_degrees += 22.5 * direction

    # this can likely be accomplished by clamping, but want to wrap around to 0 or 360
    if current_rotation_degrees >= 360:
        current_rotation_degrees -= 360
    elif current_rotation_degrees <= 0:
        current_rotation_degrees += 360

    update_rotation()
