extends KinematicBody

signal rekd

var map_indicator
var resource_type = ''
var scaling = {
    "Tree":   {"min": 0.5, "max": 0.9},
    "Stone":  {"min": 1.0, "max": 2.0},
    "Iron":   {"min": 1.0, "max": 2.0},
    "Copper": {"min": 1.0, "max": 2.0},
}
var default_scaling = {"min": 1.0, "max": 1.0}
var attackable = true


func attacked():
    handle_attacked_animation()
    # attackable = false


func handle_attacked_animation():
    """
    All objects generated from this scene will share
        an AnimationPlayer reference.
    This means changing the animation for one scene instance
        will change it for all instances.
    To avoid this:
        * the AnimationPlayer is duplicated
        * the rotation animation of the duplicate is updated
        * the original is removed
        * the new AnimationPlayer is added to the node
        * the animation is played
    """
    var old_animation_player = get_node("AnimationPlayer")
    if old_animation_player.is_playing():
        # this avoids resetting the animation
        return

    # duplicate the AnimationPlayer
    var animation_player = old_animation_player.duplicate()

    # this allows the duplicate to retain the original name of "AnimationPlayer"
    #   when added as a child of the current node
    # remove_child will not free the object
    # it will just remove the object from the current node
    remove_child(old_animation_player)

    # update the wobble keyframes with the object's current y rotation
    var wobble_animation = animation_player.get_animation("wobble")
    for keyframe in wobble_animation.track_get_key_count(0):
        wobble_animation.track_set_key_value(
            0, keyframe,
            Vector3(
                0,
                rotation_degrees.y,
                wobble_animation.track_get_key_value(0, keyframe).z
            )
        )

    # add the AnimationPlayer copy to the node
    add_child(animation_player)
    # play the animation
    animation_player.play("wobble", -1, 1)


func destroy():
    queue_free()


func _on_AnimationPlayer_animation_finished(anim_name):
    emit_signal("rekd")
    destroy()


func initialize(object_type, spawn_position):
    translation = spawn_position

    var rng = RandomNumberGenerator.new()
    rng.randomize()
    var scale_range = scaling.get(object_type, default_scaling)
    var scale_amount = rng.randf_range(scale_range.min, scale_range.max)
    set_scale(Vector3(scale_amount, scale_amount, scale_amount))
