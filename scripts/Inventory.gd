extends ColorRect

export (PackedScene) var inventory_cell_scene = preload("res://scenes/InventorySlot2D.tscn")
export (PackedScene) var inventory_cell_meshes = preload("res://scenes/ItemMeshes.tscn")


var inventory = {}
var cells = {}

export var inventory_type = ''
export var cell_rows = 5
export var cell_columns = 7
# determine these dynamically; the screen is 15 cells x 11 cells
export (int) var inventory_x_offset
export (int) var inventory_y_offset
# determines whether actions such as show/hide trigger when part of an inventory group
export (bool) var is_nested = false

onready var mouse_item_cell = get_tree().get_root().get_node("Main").get_node("MouseItemCell")
onready var mouse_item_cell_label = mouse_item_cell.get_node("Display").get_node("Label")
onready var mouse_item_cell_items = mouse_item_cell.get_node("Display").get_node("Items")
onready var parent_name = get_parent().name

# current_cell_resource is the name of the resource in the inventory slot the mouse is over
# this may need to change if stack size is ever capped
var current_cell_resource = ''
# current_cell_name is the name of the cell node the mouse is over
var current_cell_name = ''

var disallowed_generation_types = ['Build', 'Processing']

# possibly add a var that would disallow adding to inventory via clicks
# for things such as crafting station outputs


func _ready():
    """Set up data structures, generate cells, and connect signals."""
    randomize()

    # generate cell tracking keys
    for y in cell_rows:
        for x in cell_columns:
            cells['%s,%s' % [x, y]] = ''

    # try to set position of placeholder viewport for mouse
    mouse_item_cell.position = Vector2(0, 0)
    mouse_item_cell.visible = true
    self.rect_size = Vector2(cell_columns * 64, cell_rows * 64)
    self.rect_position = Vector2(
        self.rect_position.x + 64 * inventory_x_offset,
        self.rect_position.y + 64 * inventory_y_offset
    )

    for entry in cells:
        var dims = entry.split(',')
        var cell = inventory_cell_scene.instance()
        cell.position = Vector2(int(dims[0]) * 64, int(dims[1]) * 64)
        cell.name = entry
        # connect to mouse_entered and mouse_exited signals of target cell
        cell.connect("mouse_entered_cell", self, "_on_Cell_mouse_entered", [cell.name, parent_name])
        cell.connect("mouse_exited_cell", self, "_on_Cell_mouse_exited", [cell.name, parent_name])
        # apply inventory_cell_meshes scene to cell scene; without this the cells will be identical
        #   and it will not be possible to control them independently
        #   (unless there's a method to make an instantiated scene local to another scene via code)
        var item_meshes = inventory_cell_meshes.instance()
        cell.get_node("Display").add_child(item_meshes)
        # add the cell to the Cells collection in the inventory
        get_node("Cells").add_child(cell)

    if inventory_type == 'Player':
        add_starting_inventory()
    elif inventory_type == 'Build':
        add_build_inventory()


func _input(event):
    """Handle input trigger events."""
    if event.is_action_pressed("menu_close") and not is_nested:
        set_visible(false)

    if not self.visible:
        return

    if event is InputEventMouseButton and event.button_index == 1 and event.pressed and current_cell_name != '':
        set_mouse_item()


func add_starting_inventory():
    """Add initial inventory."""
    var starter_items = ['Stone', 'Copper', 'Iron', 'Tree']

    for n in rand_range(1, 15):
        var new_item_name = starter_items[randi() % starter_items.size()]
        add_new_inventory(new_item_name)

    update_inventory()


func add_build_inventory():
    """Add builder icons."""
    var starter_items = get_node("../BuilderScenes").get_children()
    for item in starter_items:
        add_new_inventory(item.name)

    update_inventory()


func update_inventory():
    """Update cells with data from inventory."""
    for child in get_node("Cells").get_children():
        clear_container_details(child)

    for entry in inventory:
        var target = inventory.get(entry).get('slot')
        if target:
            # find_node will get every sub node that matches
            # get_node will get only children of current
            # unable to use either to find generated scene nodes
            # in this case in particular; unsure why
            # used get_children and name match instead
            var container = null
            for child in get_node("Cells").get_children():
                if child.name == target:
                    container = child
                    break

            if container:
                set_container_details(container, inventory.get(entry))
            else:
                print('no child found for ', target)


func set_mouse_item():
    """Set mouse item when inventory cell is clicked."""
    print('at set mouse item, resource is "', current_cell_resource, '", name is "', current_cell_name, '"')
    if mouse_item_cell.mouse_item == null:
        print('attempting to grab item "%s" from inventory' % current_cell_resource)
        # grab the item from the cell
        if not current_cell_resource:
            print("no current cell resource")
            return

        # hide all meshes
        for child in mouse_item_cell_items.get_children():
            child.visible = false

        if inventory_type == 'Build':
            handle_build_object()
            return

        # pick object from inventory cell into mouse_item
        mouse_item_cell.mouse_item = remove_from_inventory(current_cell_resource)

        if mouse_item_cell.mouse_item != null:
            set_container_details(mouse_item_cell, mouse_item_cell.mouse_item)
            current_cell_resource = ''

            # call clear container with child ref instead of entire inventory update
            update_inventory()
            mouse_item_cell.position = get_viewport().get_mouse_position() - Vector2(32, 32)
            print('clicked on "%s"; found %s' % [mouse_item_cell.mouse_item.get('name'), mouse_item_cell.mouse_item.get('count')])
    else:
        # put the item in the cell
        if current_cell_resource:
            # swap cells here
            print('attempting to swap inventory item "%s" and mouse item "%s"' % [current_cell_resource, mouse_item_cell.mouse_item.get('name')])
            mouse_item_cell.mouse_swap_item = remove_from_inventory(current_cell_resource)
            # TODO: should log an error here because we're over a cell that should have a resource
            #   but we didn't get one
            if mouse_item_cell.mouse_swap_item == null:
                return

            # This will combine mouse_swap_item and mouse_item if they are the same type
            mouse_item_cell.check_swap_item()

            set_inventory_with_mouse_item()
            update_inventory()

            if mouse_item_cell.mouse_swap_item != null:
                mouse_item_cell.mouse_item = mouse_item_cell.mouse_swap_item
                mouse_item_cell.mouse_swap_item = null
                set_container_details(mouse_item_cell, mouse_item_cell.mouse_item)
        else:
            # target cell is empty, so update it with current details
            print('attempting to insert mouse item "%s" into inventory' % mouse_item_cell.mouse_item.get('name'))
            set_inventory_with_mouse_item()
            update_inventory()


func handle_build_object():
    get_parent().select_build_object(current_cell_resource)
    get_parent().set_build_inventory_mode(false)


func remove_from_inventory(item_name, count=-1):
    """Remove item from inventory data."""
    var item = inventory.get(item_name)
    if not item:
        return
    if count < 0 or count == item.get('count'):
        var cell = item.get('slot')
        cells[cell] = ''
        inventory.erase(item_name)
        return item
    else:
        item.count -= count
        return {'name': item.get('name'), 'count': count, 'slot': item.get('slot')}


func set_container_details(container, item):
    """Update cell node with item details."""
    var display = container.get_node("Display")
    if inventory_type != 'Build':
        var label = display.get_node("Label")
        label.text = "%s" % item.get('count')
    var items = display.get_node("Items")
    print(item)
    items.get_node(item.get('name')).visible = true
    items.hint_tooltip = item.get('name')


func set_inventory_with_mouse_item():
    """Update inventory with current mouse item."""
    var updated = set_inventory(mouse_item_cell.mouse_item)
    if updated:
        mouse_item_cell.mouse_item = null
        clear_container_details(mouse_item_cell)


func set_inventory(item, automation_source=false):
    """Update inventory cell data structures with item details."""
    if not current_cell_name and not automation_source:
        print('nope, returning')
        return false

    var target_cell = current_cell_name if current_cell_name else '0,0'

    print('updating inventory')

    var item_name = item.get('name')
    var inventory_item = remove_from_inventory(item_name)
    if inventory_item != null:
        item['count'] += inventory_item.get('count')

    inventory[item_name] = item

    cells[target_cell] = item_name
    current_cell_resource = item_name
    item['slot'] = target_cell
    update_inventory()
    return true


func clear_container_details(container):
    """Clear item details from cell container."""
    var display = container.get_node("Display")
    var items = display.get_node("Items")
    for child in items.get_children():
        child.visible = false
    items.hint_tooltip = ''
    display.get_node("Label").text = ''


func _on_Item_collected(item_name):
    """Update inventory layout when an item is collected."""
    add_new_inventory(item_name)
    update_inventory()


func add_new_inventory(item_name):
    """Create new inventory entry."""
    var item = inventory.get(item_name, {'name': item_name, 'count': 0, 'slot': ''})
    var mod_item = set_cell(item)
    if not mod_item:
        return
    mod_item['count'] += 1
    inventory[item_name] = mod_item


func set_cell(item):
    """Assign a cell to the item if it doesn't have one."""
    # make sure item doesn't already have a cell assigned
    var slot = item.get('slot', '')
    if slot:
        return item

    # assign an unused slot if available
    for entry in cells:
        if not cells[entry]:
            item['slot'] = entry
            cells[entry] = item.get('name')
            return item

    return {}


func _on_Cell_mouse_entered(name, parent):
    """Handle signal emitted by mouse entering an inventory cell."""
    if parent != parent_name:
        return
    current_cell_resource = cells.get(name)
    current_cell_name = name
    print('entering, resource is "', current_cell_resource, '", name is "', current_cell_name, '"')


func _on_Cell_mouse_exited(name, parent):
    """Handle signal emitted by mouse exiting an inventory cell."""
    if parent != parent_name:
        return
    reset_cell_trackers()
    print('exiting, resource is "', current_cell_resource, '", name is "', current_cell_name, '"')


func reset_cell_trackers():
    current_cell_resource = ''
    current_cell_name = ''


func toggle():
    self.visible = !self.visible
    if not self.visible:
        reset_cell_trackers()
    return self.visible


func set_visible(state):
    self.visible = state
    if not self.visible:
        reset_cell_trackers()
    return self.visible
