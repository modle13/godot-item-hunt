extends KinematicBody

signal rekd


func attacked():
    get_node("AnimationPlayer").play("wobble", -1, 1)


func destroy():
    queue_free()


func _on_AnimationPlayer_animation_finished(anim_name):
    print('anim exited in house')
    emit_signal("rekd")
    destroy()
