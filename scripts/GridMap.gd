extends GridMap


func _ready():
    generate_meshes()


func generate_meshes():
    var rng = RandomNumberGenerator.new()
    rng.randomize()
#    clear()
#    for n in range(-100, 100):
#        for m in range(-100, 100):
#            var target_mesh = rng.randi_range(0, 5)
#            set_cell_item(
#                n,
#                -1,
#                m,
#                target_mesh
#            )


func _input(event):
    # toggle_debug binding is 'R' (Project > Project Settings > Input Map)
    if Input.is_action_just_pressed("shuffle"):
        print("triggered gridmap shuffle")
        generate_meshes()
