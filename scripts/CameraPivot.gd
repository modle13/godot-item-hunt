extends Spatial

export var inverse = true

const LOOK_SENSITIVITY : float = 15.0

var far_distance : float = 200.0

var mouse_delta : Vector2 = Vector2.ZERO

var capture_camera_movement = false
var initial_position = Vector2.ZERO

onready var player = get_parent()
onready var build_items_node = player.get_node("BuildItems")
onready var compass = player.get_parent().get_node("Compass/ColorRect")

const CLAMP_X = {'max': -50, 'min': 15}

var movement_active = true


func _ready():
    rotation_degrees.x = clamp(rotation_degrees.x, CLAMP_X.get('max'), CLAMP_X.get('min'))
    Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
    $Camera.far = far_distance


func _input(event):
    if event is InputEventMouseMotion and movement_active:
        mouse_delta = event.relative


func _physics_process(delta):
    handle_rotational_movement(delta)
    handle_transform_movement()


func handle_rotational_movement(delta):
    """Rotate player facing around Y axis; rotate camera around X axis within clamp range."""
    var rotation_adjust = Vector3(mouse_delta.y, mouse_delta.x, 0) * LOOK_SENSITIVITY * delta
    if inverse:
        rotation_degrees.x -= rotation_adjust.x
    else:
        rotation_degrees.x += rotation_adjust.x
    rotation_degrees.x = clamp(rotation_degrees.x, CLAMP_X.get('max'), CLAMP_X.get('min'))

    player.rotation_degrees.y -= rotation_adjust.y
    compass.rect_rotation = player.rotation_degrees.y

    mouse_delta = Vector2.ZERO


func handle_transform_movement():
    """Control zoom distance of camera."""
    # turn off zoom if in build mode (currently contained in the Main.gd script)
    if build_items_node.build_mode:
        return

    var zoom = 0.0

    if Input.is_action_just_released("wheel_up"):
        zoom -= 0.5
    if Input.is_action_just_released("wheel_down"):
        zoom += 0.5

    transform.origin.y = max(0, transform.origin.y + zoom)
    transform.origin.z = max(0, transform.origin.z + zoom)
