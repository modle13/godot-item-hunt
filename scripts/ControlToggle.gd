extends Control


func toggle():
    self.visible = !self.visible

    for n in get_children():
        n.set_visible(self.visible)

    return self.visible


func set_visible(state):
    self.visible = state
    for n in get_children():
        n.set_visible(self.visible)
    return self.visible


func _input(event):
    """Handle input trigger events."""
    if event.is_action_pressed("menu_close"):
        set_visible(false)
