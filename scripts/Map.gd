extends ColorRect

export (PackedScene) var map_cell_scene = preload("res://scenes/MapCell.tscn")

onready var player_node = get_node("../Player")
onready var player_display = get_node("Actors/PlayerDisplay")
onready var player_direction = get_node("Actors/PlayerDisplay/Direction")
var offset = Vector2(650, 150)
var cell_opacity = 0.90
var colors = {
    "Tree":   Color(0.13, 0.55, 0.13, cell_opacity), # forest green
    "Iron":   Color(0.37, 0.62, 0.63, cell_opacity), # cadet blue
    "Stone":  Color(0.75, 0.75, 0.75, cell_opacity), # grey
    "Copper": Color(0.8,  0.52, 0.25, cell_opacity),  # peru
}
var default_color = Color(0.86, 0.08, 0.24, 1) # crimson
var skip_add_on_create = ["Ground"]

func _ready():
    return
    var ground_children = get_node("../Environment/Grounds").get_children()
    for child_index in ground_children.size():
        var child = ground_children[child_index]
        var child_color = child.get_node("MeshInstance").get_surface_material(0).albedo_color
        child_color.a = 0.7
        var position = Vector2(child.translation.x, child.translation.z)
        var size = child.get_node("MeshInstance").mesh.size
        # adjust up/left
        var adjustment = Vector2(size.x / 2, size.y / 2)
        position -= adjustment
        var new_rect = add_object("Ground", position, size, child_color)
        get_node("Grounds").add_child(new_rect)


func _process(delta):
    # update player rect position
    player_display.rect_position = Vector2(
        player_node.translation.x, player_node.translation.z
    ) + offset

    # update player direction indicator rect position
    # get offset based on sizes of player and indicator rects
    var direction_offset = \
        Vector2(player_display.rect_size.x/2, player_display.rect_size.y/2) - \
        Vector2(player_direction.rect_size.x/2, player_direction.rect_size.y/2)
    # determine direction scaling based on size of player indicator rect
    var direction_scale = Vector2(player_display.rect_size.x, player_display.rect_size.y)
    # set player direction rect position to center of player indicator rect
    player_direction.rect_position = Vector2(player_node.direction.x, player_node.direction.z)
    # multiply by scale factor
    player_direction.rect_position *= direction_scale
    # add directional offset
    player_direction.rect_position += direction_offset


func add_object(object_type, position, size, object_color=null):
    var new_position = position + offset
    # instantiate map cell
    var new_map_cell = map_cell_scene.instance()

    # set position, name, color
    new_map_cell.rect_position = new_position
    new_map_cell.rect_size = size
    new_map_cell.name = "%s%d,%d" % [object_type, position.x, position.y]
    if object_color == null:
        object_color = colors.get(object_type, default_color)
    new_map_cell.color = object_color

    # add to Map
    if not skip_add_on_create.has(object_type):
        get_node("Resources").add_child(new_map_cell)
    return new_map_cell


func toggle():
    self.visible = !self.visible
    return self.visible
