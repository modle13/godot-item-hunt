extends StaticBody

onready var inventory_in = get_node("Inventory/InventoryIn")
onready var inventory_out = get_node("Inventory/InventoryOut")
onready var progress_rect = inventory_in.get_node("Cells").get_node("0,0").get_node("Progress")
onready var parent_parent_name = get_parent().get_parent().name

var collided_count = 0
var object_collided = false
var starting_y = 0

var processing_data = {
    "Iron": "IronBar",
    "Copper": "CopperBar",
}

var progress = 0

# TODO: need to detect if input inventory was changed, and reset accordingly


func _ready():
    progress_rect.visible = true
    reset_progress()
    if parent_parent_name == 'BuildItems':
        starting_y = translation.y
        translation.y = 0


func _process(delta):
    var consumes = ''
    var produces = ''
    if inventory_in.inventory:
        # if input inventory exists, it is checked against known processing data
        for key in inventory_in.inventory.keys():
            # if processing data doesn't contain input inventory key
            # then nothing will happen
            # this logic will only work for single input item processing
            consumes = key
            produces = processing_data.get(key)
            break
    else:
        reset_progress()
        progress = 0

    if produces:
        # if there's a match for the item in the inventory_in inventory
        # process will be attempted
        if inventory_out.inventory:
            # if no keys exist, this code will not trigger at all
            for key in inventory_out.inventory.keys():
                # if key exists and don't match, return
                if key != produces:
                    reset_progress()
                    return

        # checks passed, so increment progress
        progress += delta * 18
        progress = min(100, progress)
        if int(progress) % 4 == 0:
            progress_rect.rect_position.y = 60 - int(progress / 100 * 56)
            progress_rect.rect_size.y = int(progress / 100 * 56)
        if progress >= 100:
            produce(consumes, produces)


func initialize(spawn_position):
    translation = spawn_position


func reset_progress():
    progress_rect.rect_size.y = 0
    progress = 0


func produce(consumes, produces):
    var removed_item = inventory_in.remove_from_inventory(consumes, 1)
    inventory_in.update_inventory()
    inventory_out.set_inventory({'name': produces, 'count': removed_item.get('count'), 'slot': '0,0'}, true)
    reset_progress()


func toggle(activated):
    print('toggling ', name)
    pass


func _on_ObjectDetector_body_entered(body):
    """Detects that other objects have entered ObjectDetector. Used for Placement Only."""
    if parent_parent_name != 'BuildItems':
        return
    # need to use counts because multiple bodies can be entered and a single
    #   exit would cause the additional bodies to then be ignored
    collided_count += 1
    object_collided = collided_count > 0


func _on_ObjectDetector_body_exited(body):
    """Detects that other objects have entered ObjectDetector. Used for Placement Only."""
    if parent_parent_name != 'BuildItems':
        return
    collided_count -= 1
    object_collided = collided_count > 0
