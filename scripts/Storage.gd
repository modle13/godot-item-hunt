extends StaticBody


var collided_count = 0
var object_collided = false
var starting_y = 0
onready var parent_parent_name = get_parent().get_parent().name


func _ready():
    if parent_parent_name == 'BuildItems':
        starting_y = translation.y
        translation.y = -20


func initialize(spawn_position):
    translation = spawn_position


func toggle(activated):
    print('toggling ', name)
    if activated:
        open()
    else:
        close()


func open():
    get_node("Open").visible = true
    get_node("Closed").visible = false


func close():
    get_node("Open").visible = false
    get_node("Closed").visible = true


func _on_ObjectDetector_body_entered(body):
    """Detects that other objects have entered ObjectDetector. Used for Placement Only."""
    if parent_parent_name != 'BuildItems':
        return
    # need to use counts because multiple bodies can be entered and a single
    #   exit would cause the additional bodies to then be ignored
    collided_count += 1
    object_collided = collided_count > 0


func _on_ObjectDetector_body_exited(body):
    """Detects that other objects have entered ObjectDetector. Used for Placement Only."""
    if parent_parent_name != 'BuildItems':
        return
    collided_count -= 1
    object_collided = collided_count > 0
