extends KinematicBody


var collided_count = 0
var object_collided = false
var starting_y = 0


func _ready():
    if get_parent().get_parent().name == 'BuildItems':
        starting_y = translation.y
        translation.y = -20


func initialize(spawn_position):
    translation = spawn_position


func toggle(activated):
    pass


func _on_ObjectDetector_body_entered(body):
    """Detects that other objects have entered ObjectDetector."""
    if get_parent().get_parent().name != 'BuildItems':
        return
    # need to use counts because multiple bodies can be entered and a single
    #   exit would cause the additional bodies to then be ignored
    collided_count += 1
    object_collided = collided_count > 0


func _on_ObjectDetector_body_exited(body):
    """Detects that other objects have exited ObjectDetector."""
    if get_parent().get_parent().name != 'BuildItems':
        return
    collided_count -= 1
    object_collided = collided_count > 0
